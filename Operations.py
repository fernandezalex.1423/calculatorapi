class Operations():
    def __init__(self, a, b):
        self.a = a
        self.b = b

    def addition(self, a, b):
        return a + b

    def difference(self, a, b):
        return a - b

    def product(self, a, b):
        return a * b

    def quotient(self, a, b):
        return a / b